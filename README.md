# TASK MANAGER

SCREENSHOTS

https://drive.google.com/drive/folders/1NVrzH6noO6bkUO_uMTGHWh7D0bAFbq5A?usp=sharing

## DEVELOPER INFO

name: Danil Kitaev

e-mail: danil.kitaev1996@gmail.com

e-mail: dkitaev@tsconsulting.com

## HARDWARE

CPU: i5

RAM: 6G

SSD: 90GB

## SOFTWARE

System: Windows 10 Version 1909

Version JDK: 1.8.0_282

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
