package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.model.Session;

public interface ISessionRepository extends IRepository<Session>{

    boolean exists(@NotNull final String id);

}
