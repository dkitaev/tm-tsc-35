package ru.tsc.kitaev.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.entity.IHasCreated;

import java.util.Comparator;
import java.util.Objects;

@NoArgsConstructor
public final class ComparatorByCreated implements Comparator<IHasCreated> {

    @NotNull
    private static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    @NotNull
    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return Objects.requireNonNull(o1.getCreated()).compareTo(o2.getCreated());
    }

}
