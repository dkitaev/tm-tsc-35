package ru.tsc.kitaev.tm.exception.user;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class UserIsLockedException extends AbstractException {

    public UserIsLockedException () {
        super("Error! User is locked...");
    }

}
