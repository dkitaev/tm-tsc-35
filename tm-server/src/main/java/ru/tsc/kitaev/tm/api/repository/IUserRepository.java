package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User removeUser(@NotNull User user);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

    Boolean isLoginExists(@NotNull final String login);

    Boolean isEmailExists(@NotNull final String email);

}
