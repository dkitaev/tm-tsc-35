package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.user.EmailExistsException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (userRepository.isLoginExists(login)) throw new LoginExistsException();
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public void isEmailExists(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (userRepository.isEmailExists(email)) throw new EmailExistsException();
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        isEmailExists(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        setPassword(user, password);
    }

    @Override
    public void setPassword(@Nullable final User user, @Nullable final String password) {
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

}
