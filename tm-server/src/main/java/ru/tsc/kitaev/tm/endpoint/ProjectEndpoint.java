package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Project entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Project entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable String sort
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId(), sort);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @WebMethod
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @Override
    @NotNull
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @NotNull
    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @NotNull
    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }

}
