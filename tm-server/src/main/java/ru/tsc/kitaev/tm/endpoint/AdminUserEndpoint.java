package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User findByEmail(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByEmail(email);
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUser(user);
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public User createUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
