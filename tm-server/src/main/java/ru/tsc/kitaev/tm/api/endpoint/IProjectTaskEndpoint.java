package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectTaskEndpoint {

    @NotNull
    @WebMethod
    List<Task> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    );

    @Nullable
    @WebMethod
    Task bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    );

    @Nullable
    @WebMethod
    Task unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    );

    @Nullable
    @WebMethod
    Project removeById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    );

    @Nullable
    @WebMethod
    Project removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Project removeByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "nam", partName = "nam") @Nullable String name
    );

}
