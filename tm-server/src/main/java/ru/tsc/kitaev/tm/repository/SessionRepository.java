package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.model.Session;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean exists(@NotNull final String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }

}
