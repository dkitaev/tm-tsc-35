package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Project entity
    );

    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Project entity
    );

    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    public List<Project> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable String sort
    );

    @Nullable
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @Nullable
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @NotNull
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @NotNull
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @Nullable
    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @NotNull
    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    );

    @NotNull
    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @NotNull
    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @NotNull
    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    );

    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable int index
    );

}
