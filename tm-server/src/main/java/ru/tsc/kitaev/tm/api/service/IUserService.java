package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    void isLoginExists(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    void isEmailExists(@Nullable String email);

    @Nullable
    User removeUser(@Nullable User user);

    void removeByLogin(@Nullable String login);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String userId, @Nullable String password);

    void setPassword(@NotNull User user, @NotNull String password);

    void updateUser(@Nullable String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
