package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Task bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @Nullable
    @WebMethod
    public Task unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeById(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeByName(session.getUserId(), name);
    }

}
