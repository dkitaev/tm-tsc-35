package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.User;

import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public static Predicate<User> predicateByLogin(@NotNull final String login) {
        return s -> s.getLogin().equals(login);
    }

    @NotNull
    public static Predicate<User> predicateByEmail(@NotNull final String email) {
        return s -> s.getEmail().equals(email);
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(predicateByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        removeUser(user);
    }

    @NotNull
    public Boolean isLoginExists(@NotNull final String login) {
        return list.stream().anyMatch(s -> s.getLogin().equals(login));
    }

    @NotNull
    public Boolean isEmailExists(@NotNull final String email) {
        return list.stream().anyMatch(s -> s.getEmail().equals(email));
    }

}
