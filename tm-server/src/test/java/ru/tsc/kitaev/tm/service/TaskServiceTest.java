package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.IProjectTaskService;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.ProjectRepository;
import ru.tsc.kitaev.tm.repository.TaskRepository;
import ru.tsc.kitaev.tm.util.HashUtil;

public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTaskDescription";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testTask";

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final String userId;

    public TaskServiceTest() {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        task = new Task();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(userId, task);
        taskService = new TaskService(taskRepository);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription("testProjectDescription");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        projectRepository.add(userId, project);
        projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    }

    @Test
    public void createTest() {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        @NotNull final Task newTask = taskService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertEquals(newProjectName, newTask.getName());
        Assert.assertEquals(newProjectDescription, newTask.getDescription());
    }

    @Test
    public void findByProjectTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task, taskService.findById(userId, taskId));
        Assert.assertEquals(task, taskService.findByIndex(userId, 0));
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
    }

    @Test
    public void updateByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskDescription);
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        taskService.updateById(userId, taskId, newProjectName, newProjectDescription);
        Assert.assertEquals(task, taskService.findByName(userId, newProjectName));
        @NotNull final Task newTask = taskService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskId, newTask.getId());
        Assert.assertEquals(newProjectName, newTask.getName());
        Assert.assertEquals(newProjectDescription, newTask.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskDescription);
        Assert.assertEquals(task, taskService.findById(userId, taskId));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        taskService.updateByIndex(userId, 0,newProjectName, newProjectDescription);
        Assert.assertEquals(task, taskService.findByIndex(userId, 0));
        @NotNull final Task newTask = taskService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskId, newTask.getId());
        Assert.assertEquals(newProjectName, newTask.getName());
        Assert.assertEquals(newProjectDescription, newTask.getDescription());
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.removeById(userId, taskId);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void removeTaskByIndexTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        taskService.removeByIndex(userId, 0);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void removeTaskByNameTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.removeByName(userId, taskName);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.startById(userId, taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        taskService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.startByName(userId, taskName);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByName(userId, taskName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.finishById(userId, taskId);
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.finishByName(userId, taskName);
        Assert.assertEquals(Status.COMPLETED, taskService.findByName(userId, taskName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(userId, taskId).getStatus());
        taskService.changeStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskId).getStatus());
        taskService.changeStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        taskService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByIndex(userId, 0).getStatus());
        taskService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findByIndex(userId, 0).getStatus());
        taskService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByName(userId, taskName).getStatus());
        taskService.changeStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findByName(userId, taskName).getStatus());
        taskService.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findByName(userId, taskName).getStatus());
    }

    @Test
    public void findTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(task, projectTaskService.findTaskByProjectId(userId, projectId).get(0));
    }

    @Test
    public void bindTaskById() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void unbindTaskById() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskService.findById(userId, taskId).getProjectId());
        projectTaskService.unbindTaskById(userId, projectId, taskId);
        Assert.assertNull(taskService.finishById(userId, taskId).getProjectId());
    }

    @Test
    public void removeProjectByIdTest() {
        bindTaskById();
        Assert.assertEquals(1, projectTaskService.findTaskByProjectId(userId, projectId).size());
        projectTaskService.removeById(userId, projectId);
        Assert.assertEquals(0, projectTaskService.findTaskByProjectId(userId, projectId).size());
    }

    @Test
    public void removeProjectByIndexTest() {
        bindTaskById();
        Assert.assertEquals(1, projectTaskService.findTaskByProjectId(userId, projectId).size());
        projectTaskService.removeByIndex(userId, 0);
        Assert.assertEquals(0, projectTaskService.findTaskByProjectId(userId, projectId).size());
    }

    @Test
    public void removeProjectByNameTest() {
        bindTaskById();
        Assert.assertEquals(1, projectTaskService.findTaskByProjectId(userId, projectId).size());
        projectTaskService.removeByName(userId, projectName);
        Assert.assertEquals(0, projectTaskService.findTaskByProjectId(userId, projectId).size());
    }

}
