package ru.tsc.kitaev.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class AdminUserEndpointTest {

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String userLogin = "man";

    @NotNull
    private Session session;

    @Nullable
    private String userId;

    @NotNull
    private User user;

    public AdminUserEndpointTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("admin", "admin");
        user = adminUserEndpoint.createUser(session, userLogin, "man", "man@mail.com");
        userId = user.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findByLoginTest() {
        @NotNull final User buffUser = adminUserEndpoint.findByLogin(session, userLogin);
        Assert.assertNotNull(buffUser);
        Assert.assertEquals(userLogin, buffUser.getLogin());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByEmailTest() {
        @NotNull final User buffUser = adminUserEndpoint.findByEmail(session, "man@mail.com");
        Assert.assertNotNull(buffUser);
        Assert.assertEquals("man@mail.com", buffUser.getEmail());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByLoginTest() {
        Assert.assertNotNull(adminUserEndpoint.findByLogin(session, userLogin));
        adminUserEndpoint.removeByLogin(session, userLogin);
        userId = null;
        Assert.assertNull(adminUserEndpoint.findByLogin(session, userLogin));
    }

    @Test
    @Category(SoapCategory.class)
    public void createUserTest() {
        @NotNull final String newUserLogin = "devil";
        Assert.assertNotNull(adminUserEndpoint.createUser(
                session,
                newUserLogin,
                "devil",
                "devil@mail.com"
        ));
        Assert.assertNotNull(adminUserEndpoint.findByLogin(session, newUserLogin));
        adminUserEndpoint.removeByLogin(session, newUserLogin);
    }

    @Test
    @Category(SoapCategory.class)
    public void lockUnlockUserByLoginTest() {
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, userLogin);
        Assert.assertTrue(adminUserEndpoint.findByLogin(session, userLogin).isLocked());
        adminUserEndpoint.unlockUserByLogin(session, userLogin);
        Assert.assertFalse(adminUserEndpoint.findByLogin(session, userLogin).isLocked());
    }

    @After
    public void after() {
        if (userId != null) adminUserEndpoint.removeByLogin(session, userLogin);
        sessionEndpoint.closeSession(session);
    }

}
