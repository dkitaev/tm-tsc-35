package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class ProjectTaskEndpointTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final String taskName = "taskTest";

    @Nullable
    private String taskId;

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @Nullable
    private String projectId;

    @NotNull
    private final String projectName = "project";

    @NotNull
    private Session session;

    public ProjectTaskEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        task = taskEndpoint.createTask(session, taskName, "taskDescription");
        taskId = task.getId();
        project = projectEndpoint.createProject(session, projectName, "projectDescription");
        projectId = project.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByProjectIdTest() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        @NotNull final String newTaskId = taskEndpoint.createTask(session, "task2", "task2").getId();
        projectTaskEndpoint.bindTaskById(session, projectId, newTaskId);
        Assert.assertEquals(2, projectTaskEndpoint.findTaskByProjectId(session, projectId).size());
        taskEndpoint.removeTaskById(session, newTaskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void bindTaskByIdTest() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void unbindTaskByIdTest() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
        projectTaskEndpoint.unbindTaskById(session, projectId, taskId);
        Assert.assertNull(taskEndpoint.findTaskById(session, taskId).getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        projectTaskEndpoint.removeById(session, projectId);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndexTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        projectTaskEndpoint.removeByIndex(session, 0);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByNameTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        projectTaskEndpoint.removeByName(session, projectName);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
    }

    @After
    public void after() {
        if (projectId != null) projectTaskEndpoint.removeById(session, projectId);
        sessionEndpoint.closeSession(session);
    }

}
