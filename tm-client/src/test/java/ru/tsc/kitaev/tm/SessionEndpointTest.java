package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.Result;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.endpoint.SessionEndpoint;
import ru.tsc.kitaev.tm.endpoint.SessionEndpointService;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openSessionTest() {
        Assert.assertNotNull(sessionEndpoint.openSession("test", "test"));
    }

    @Test
    @Category(SoapCategory.class)
    public void closeSessionTest() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test");
        @NotNull final Result result = sessionEndpoint.closeSession(session);
        Assert.assertTrue(result.isSuccess());
    }

}
