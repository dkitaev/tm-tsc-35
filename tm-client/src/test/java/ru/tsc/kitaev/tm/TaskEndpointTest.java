package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class TaskEndpointTest {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String taskName = "taskTest";

    @NotNull
    private final String taskDescription = "taskTestDescription";

    @Nullable
    private String taskId;

    @NotNull
    private Task task;

    @NotNull
    private Session session;

    public TaskEndpointTest() {
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        task = taskEndpoint.createTask(session, taskName, taskDescription);
        taskId = task.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllTaskTest() {
        Assert.assertEquals(1, taskEndpoint.findAllTask(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskTest() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findTaskById(session, taskId));
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findTaskByName(session, taskName));
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(session, 0));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByIdTest() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findTaskById(session, taskId));
        taskEndpoint.removeTaskById(session, taskId);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByIndexTest() {
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(session, 0));
        taskEndpoint.removeTaskByIndex(session, 0);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByIdNameTest() {
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findTaskByName(session, taskName));
        taskEndpoint.removeTaskByName(session, taskName);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        @NotNull final String buffTaskId =
                taskEndpoint.createTask(session, "taskTest", "taskTest").getId();
        Assert.assertNotNull(buffTaskId);
        Assert.assertNotNull(taskEndpoint.findTaskById(session, buffTaskId));
        taskEndpoint.removeTaskById(session, buffTaskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdTaskTest() {
        Assert.assertNotNull(taskId);
        @NotNull final String newProjectName = "newTaskName";
        @NotNull final String newProjectDescription = "newTaskDescription";
        taskEndpoint.updateTaskById(session, taskId, newProjectName,newProjectDescription);
        @NotNull final Task updatedTask = taskEndpoint.findTaskById(session, taskId);
        Assert.assertEquals(taskId, updatedTask.getId());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
        Assert.assertEquals(newProjectName, updatedTask.getName());
        Assert.assertEquals(newProjectDescription, updatedTask.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIndexTaskTest() {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        taskEndpoint.updateTaskByIndex(session, 0, newProjectName,newProjectDescription);
        @NotNull final Task updatedTask = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
        Assert.assertEquals(newProjectName, updatedTask.getName());
        Assert.assertEquals(newProjectDescription, updatedTask.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByIdTest() {
        Assert.assertNotNull(taskId);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.startTaskById(session, taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskById(session, taskId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByIndexTest() {
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.startTaskByIndex(session, 0);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByNameTest() {
        Assert.assertNotNull(taskName);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.startTaskByName(session, taskName);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskByName(session, taskName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByIdTest() {
        Assert.assertNotNull(taskId);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.finishTaskById(session, taskId);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskById(session, taskId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByIndexTest() {
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.finishTaskByIndex(session, 0);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByNameTest() {
        Assert.assertNotNull(taskName);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.finishTaskByName(session, taskName);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskByName(session, taskName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(taskId);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.changeTaskStatusById(session, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskById(session, taskId).getStatus());
        taskEndpoint.changeTaskStatusById(session, taskId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskById(session, taskId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIndexTest() {
        Assert.assertNotNull(session);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.changeTaskStatusByIndex(session, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskByIndex(session, 0).getStatus());
        taskEndpoint.changeTaskStatusByIndex(session, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByNameTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskEndpoint.changeTaskStatusByName(session, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findTaskByName(session, taskName).getStatus());
        taskEndpoint.changeTaskStatusByName(session, taskName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findTaskByName(session, taskName).getStatus());
    }

    @After
    public void after() {
        if (taskId != null) taskEndpoint.removeTaskById(session, taskId);
        sessionEndpoint.closeSession(session);
    }

}
