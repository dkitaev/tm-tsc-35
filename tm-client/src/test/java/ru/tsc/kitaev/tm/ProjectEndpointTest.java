package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.marker.SoapCategory;

public class ProjectEndpointTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String projectName = "projectTest";

    @NotNull
    private final String projectDescription = "projectTestDescription";

    @Nullable
    private String projectId;

    @NotNull
    private Project project;

    @NotNull
    private Session session;

    public ProjectEndpointTest() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        project = projectEndpoint.createProject(session, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllProjectTest() {
        Assert.assertEquals(1, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, projectId));
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findProjectByName(session, projectName));
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(session, 0));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, projectId));
        projectTaskEndpoint.removeById(session, projectId);
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(session, 0));
        projectTaskEndpoint.removeByIndex(session, 0);
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findProjectByName(session, projectName));
        projectTaskEndpoint.removeByName(session, projectName);
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        @NotNull final String buffProjectId =
                projectEndpoint.createProject(session, "newTest", "newTest").getId();
        Assert.assertNotNull(buffProjectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, buffProjectId));
        projectTaskEndpoint.removeById(session, buffProjectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdProjectTest() {
        Assert.assertNotNull(projectId);
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectEndpoint.updateProjectById(session, projectId, newProjectName,newProjectDescription);
        @NotNull final Project updatedProject = projectEndpoint.findProjectById(session, projectId);
        Assert.assertEquals(projectId, updatedProject.getId());
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIndexProjectTest() {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectEndpoint.updateProjectByIndex(session, 0, newProjectName,newProjectDescription);
        @NotNull final Project updatedProject = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.startProjectById(session, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectById(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIndexTest() {
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.startProjectByIndex(session, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByNameTest() {
        Assert.assertNotNull(projectName);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.startProjectByName(session, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectByName(session, projectName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.finishProjectById(session, projectId);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectById(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIndexTest() {
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.finishProjectByIndex(session, 0);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByNameTest() {
        Assert.assertNotNull(projectName);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.finishProjectByName(session, projectName);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectByName(session, projectName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(projectId);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.changeProjectStatusById(session, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectById(session, projectId).getStatus());
        projectEndpoint.changeProjectStatusById(session, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectById(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIndexTest() {
        Assert.assertNotNull(session);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.changeProjectStatusByIndex(session, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectByIndex(session, 0).getStatus());
        projectEndpoint.changeProjectStatusByIndex(session, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectByIndex(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByNameTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        projectEndpoint.changeProjectStatusByName(session, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findProjectByName(session, projectName).getStatus());
        projectEndpoint.changeProjectStatusByName(session, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findProjectByName(session, projectName).getStatus());
    }

    @After
    public void after() {
        if (projectId != null) projectTaskEndpoint.removeById(session, projectId);
        sessionEndpoint.closeSession(session);
    }

}
