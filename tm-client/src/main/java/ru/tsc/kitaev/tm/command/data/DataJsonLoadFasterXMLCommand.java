package ru.tsc.kitaev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.endpoint.Session;

public class DataJsonLoadFasterXMLCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load json data from file";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().dataJsonLoadFasterXML(session);
    }

}
