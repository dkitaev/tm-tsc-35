package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.endpoint.Project;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter Index");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().removeProjectByIndex(session, index);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskEndpoint().removeById(session, project.getId());
        System.out.println("[OK]");
    }

}
