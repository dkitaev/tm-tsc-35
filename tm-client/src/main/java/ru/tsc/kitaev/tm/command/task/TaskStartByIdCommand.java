package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.endpoint.Task;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by id...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().startTaskById(session, id);
        if (task == null) throw new TaskNotFoundException();
    }

}
