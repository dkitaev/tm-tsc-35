package ru.tsc.kitaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands...";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable String commandName = command.name();
            if (commandName != null || !commandName.isEmpty())
                System.out.println(commandName + ": " + command.description());
        }
    }

}
